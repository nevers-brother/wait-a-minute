﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class OrderGoods : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //添加语句
            string sql = "insert into OrderInfo( GoodsId, UserId, OrderTime, SendType, Address, Phone)" +
                "values( @GoodsId, @UserId, @OrderTime, @SendType, @Address, @Phone)";
            //连接数据库
            using (SqlConnection con = new SqlConnection("server=ACODE-SSWNNJCQD\\SQLEXPRESS;database=GoodsManage;uid=sa;pwd=123456;"))
            {
                using (SqlCommand command = new SqlCommand(sql, con)) 
                {
                    command.Parameters.AddWithValue("UserId",txtName.Text);
                    command.Parameters.AddWithValue("GoodsId", DropDownList1.SelectedValue);
                    command.Parameters.AddWithValue("Address",txtaddress.Text);
                    command.Parameters.AddWithValue("OrderTime",DateTime.Now.ToString());
                    command.Parameters.AddWithValue("Phone",txtphone.Text);
                    command.Parameters.AddWithValue("SendType", RadioButtonList1.SelectedValue);

                    con.Open();
                    int result=command.ExecuteNonQuery();
                    if (result > 0)
                    {
                        Response.Write("<script>alert('订购成功');window.location='Order.aspx';</script>");
                    }
                    else { Response.Write("<script>alert('订购失败');</script>"); }
                    con.Close();
                }
            }
        }
    }
}