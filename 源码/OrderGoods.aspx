﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderGoods.aspx.cs" Inherits="WebApplication1.OrderGoods" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>商品名称</td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="GoodsName" DataValueField="GoodsId"></asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:GoodsManageConnectionString2 %>" SelectCommand="SELECT * FROM [GoodsInfo]"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>收货地址</td>
                    <td>
                        <asp:TextBox ID="txtaddress" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="收货地址不能为空" ControlToValidate="txtaddress"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>收货电话</td>
                    <td>
                        <asp:TextBox ID="txtphone" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="电话不能为空" ControlToValidate="txtphone"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>配送方式</td>
                    <td>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="0">自提</asp:ListItem>
                            <asp:ListItem Value="1">商家配送</asp:ListItem>
                        </asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="订购" OnClick="Button1_Click"/></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
