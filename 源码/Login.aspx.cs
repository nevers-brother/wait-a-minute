﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode=UnobtrusiveValidationMode.None;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string name=txtname.Text.Trim();
            //查询语句
            string sql = $"select * from UserInfo where UserName='{name}'";
            //链接数据库
            SqlDataAdapter adapter = new SqlDataAdapter(sql, "server=ACODE-SSWNNJCQD\\SQLEXPRESS;database=GoodsManage;uid=sa;pwd=123456");
            
            //保存数据
            DataTable dt = new DataTable();

            adapter.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                if (txtpwd.Text == dt.Rows[0]["Password"].ToString())
                {
                    Session["UserId"] = dt.Rows[0]["UserId"];

                    Response.Write("<script>alter('登录成功');window.location='/Order.aspx'</script>");
                }
                else { Response.Write("<script>alter('密码错误')</script>"); }
            }
            else { Response.Write("<script>alter('用户名不存在')</script>"); }
        }
    }
}