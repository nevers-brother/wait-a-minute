create database GoodsManage
go
use GoodsManage

create table UserInfo
(
UserId	int identity(1,1) primary key,
UserName	varchar(100) not null unique,
Password	varchar(50) not null
)

create table GoodsInfo
(
GoodsId	int identity(1,1) primary key,
GoodsName	varchar(100) not null
)
create table OrderInfo
(
OrderId	int identity(1,1) primary key,
GoodsId	int references GoodsInfo(GoodsId),
UserId	int references UserInfo(UserId),
OrderTime	dateTime not null,
State	int default('0'),--0：未发货，1：:已发货，2：已送达，3：已签收
SendType	bit default('0'),--0:商家配送，1：自提
Address	varchar(100),
Phone	Varchar(11)
)

insert into UserInfo(UserName,Password)
values('张三','123456'),('李四','123456'),('王五','123456')

insert into GoodsInfo(GoodsName) values('哈吉斯男装'),('oppo K9x 8GB+128GB 银紫超梦'),('三只松鼠零食大礼包')

insert into OrderInfo( GoodsId, UserId, OrderTime, State, SendType, Address, Phone)
values(1,1,'2021-10-10 00:00:000',1,1,'闽西学院','13648937569'),(2,2,'2021-10-10 00:00:000',2,0,'闽西学院','13648937569')

select * from OrderInfo inner join GoodsInfo on OrderInfo.GoodsId=GoodsInfo.GoodsId
inner join UserInfo on OrderInfo.UserId=UserInfo.UserId
where GoodsName like '%%';
SELECT OrderInfo.OrderId, OrderInfo.OrderTime, OrderInfo.State, OrderInfo.SendType, OrderInfo.Address, OrderInfo.Phone, UserInfo.UserName, GoodsInfo.GoodsName FROM OrderInfo INNER JOIN UserInfo ON OrderInfo.UserId = UserInfo.UserId INNER JOIN GoodsInfo ON OrderInfo.GoodsId = GoodsInfo.GoodsId